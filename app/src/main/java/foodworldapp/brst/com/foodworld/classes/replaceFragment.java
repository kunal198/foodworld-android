package foodworldapp.brst.com.foodworld.classes;

import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.fragments.SearchResultFragment;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class replaceFragment {

    public static void REPLACE_FRAGMENT(AppCompatActivity homeActivity, Fragment fragment) {



        FragmentManager fragmentManager = homeActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();



        int count = fragmentManager.getBackStackEntryCount();
        Log.d("count",count+"");

    }
}
