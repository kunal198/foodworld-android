package foodworldapp.brst.com.foodworld.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import foodworldapp.brst.com.foodworld.R;

public class SplashActivity extends AppCompatActivity {

    private int SPLASH_TME_OUT = 1200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent=new Intent(getApplication(),HomeActivity.class);
                startActivity(intent);
                finish();
            }

        }, SPLASH_TME_OUT);
    }

}
