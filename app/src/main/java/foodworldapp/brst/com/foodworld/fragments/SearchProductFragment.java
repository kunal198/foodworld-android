package foodworldapp.brst.com.foodworld.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchProductFragment extends Fragment implements View.OnClickListener{

     TextView search_tv,shopist_tv;
    SearchResultFragment searchResultFragment;
    ShoppingListFragment shoppingListFragment;
    public SearchProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_search_product, container, false);

        BIND_VIEWS(view);

        SET_LISTENER();
        return  view;
    }

    public void BIND_VIEWS(View view)
    {
        search_tv=(TextView)view.findViewById(R.id.search_tv);
        shopist_tv=(TextView)view.findViewById(R.id.shopist_tv);
        searchResultFragment=new SearchResultFragment();
        shoppingListFragment=new ShoppingListFragment();
      //  header_tv=(TextView)findViewById(R.id.header_tv);


    }


    public void SET_LISTENER()
    {
        search_tv.setOnClickListener(this);
        shopist_tv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        switch(v.getId())
        {
            case R.id.search_tv:

              /*  Intent intent=new Intent(getApplication(),SearchResultActivity.class);
                startActivity(intent);*/
                replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(),searchResultFragment);
                break;

            case R.id.shopist_tv:

                replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(),shoppingListFragment);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).LinearLayout_ll.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).header_tv.setText("CREATE YOUR SHOPPING LIST");
    }
}
