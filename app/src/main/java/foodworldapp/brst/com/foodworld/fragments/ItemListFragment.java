package foodworldapp.brst.com.foodworld.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.adaptors.ShoppingAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemListFragment extends Fragment implements ClickInterface,View.OnClickListener{

    RecyclerView listRecycler_rv;
    String name[] = {"Viceroy 750ml", "Amarula 750ml", "Golden Pilsener Lager Can 330ML",
            "Robertson Winery Natural Sweet Red 750ml", "Schweppes Lemonade 200ml",
            "J.Walker Red Label 750ml", "Chivas Regeel 12yrs"};

    ShoppingListFragment shoppingListFragment;
    ShoppingAdaptor shoppingAdaptor;

    List item = new ArrayList();
    ImageView create_iv;

    public ItemListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        BIND_VIEWS(view);

        item.clear();

        for (int i = 0; i < name.length; i++) {
            item.add(name[i]);
        }

        SET_ADAPTOR();


        return view;
    }

    private void BIND_VIEWS(View view) {

        listRecycler_rv = (RecyclerView) view.findViewById(R.id.listRecycler_rv);
        create_iv = (ImageView) view.findViewById(R.id.create_iv);
        shoppingListFragment = new ShoppingListFragment();
        /*header_tv=(TextView)findViewById(R.id.header_tv);
        header_tv.setText("MY LIST");*/
    }

    private void SET_ADAPTOR() {

        shoppingAdaptor = new ShoppingAdaptor(item);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listRecycler_rv.setLayoutManager(mLayoutManager);
        listRecycler_rv.setAdapter(shoppingAdaptor);

        shoppingAdaptor.setClickListener(this);
        create_iv.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).header_tv.setText("MY LIST");
    }

    @Override
    public void onClick(View view, int position) {


        switch (view.getId()) {


            case R.id.remove_iv:

                item.remove(position);
                shoppingAdaptor.notifyDataSetChanged();

                break;

            case R.id.inc_iv:





                break;

            case R.id.dec_iv:




                break;
        }



        //  replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),shoppingListFragment);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.create_iv:

                replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),shoppingListFragment);
                break;

        }

    }
}
