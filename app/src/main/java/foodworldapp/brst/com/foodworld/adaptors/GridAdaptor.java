package foodworldapp.brst.com.foodworld.adaptors;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.classes.searchBean;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class GridAdaptor extends BaseAdapter{

   List<searchBean> search;
    Context context;
    LayoutInflater inflater;
    public GridAdaptor(Context context, List<searchBean> searchBean) {

        this.context=context;
        this.search=searchBean;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return search.size() ;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(R.layout.custom_grid,null);
        Log.e("notifyy","noyifyy");

        TextView grid_tv=(TextView)view.findViewById(R.id.grid_tv);
        ImageView select_iv=(ImageView)view.findViewById(R.id.select_iv);

         searchBean searchBean=search.get(position);

        grid_tv.setText(searchBean.getName());

        if(searchBean.getImageVisibility())

            select_iv.setVisibility(View.VISIBLE);

        else

            select_iv.setVisibility(View.GONE);


        grid_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("postionss",""+position);
                for (int i=0;i<search.size();i++)
                {
                    if(i==position)
                    {

                    search.get(i).setImageVisibility(true);
                    }
                    else
                    {
                        search.get(i).setImageVisibility(false);
                    }
                   // searchBean=search.get(position);
                }

                for(int i=0;i<search.size();i++){
                    Log.e("visdibility",search.get(i).getImageVisibility()+"");
                }
                notifyDataSetChanged();
            }
        });





        return view;
    }
}
