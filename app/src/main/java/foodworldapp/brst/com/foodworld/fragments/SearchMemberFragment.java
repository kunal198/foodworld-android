package foodworldapp.brst.com.foodworld.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.adaptors.GridAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.classes.searchBean;


public class SearchMemberFragment extends Fragment {


    GridView grid_gv;
    String text[] = {"Luke's Phone", "Eva Andressa", "Steven Smith", "Erc Dawson"};
    searchBean searchBean;
    List<searchBean> search=new ArrayList();
    public SearchMemberFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_member, container, false);

        BIND_VIEWS(view);





        for (int i = 0; i < text.length; i++) {

            searchBean = new searchBean();

            searchBean.setName(text[i]);

            if (i == 1) {
                searchBean.setImageVisibility(true);
            } else {
                searchBean.setImageVisibility(false);
            }

            search.add(searchBean);
        }

        SET_ADAPTOR();

        return view;
    }

    private void BIND_VIEWS(View view) {

        grid_gv = (GridView) view.findViewById(R.id.grid_gv);


    }

    private void SET_ADAPTOR() {

        GridAdaptor gridAdaptor = new GridAdaptor(getContext(), search);
        grid_gv.setAdapter(gridAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).header_tv.setText("ADD MEMBERS TO YOUR LIST");
    }
}
