package foodworldapp.brst.com.foodworld.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class SearchAdaptor extends RecyclerView.Adapter<SearchAdaptor.MyViewHolder> {

    String name[];
    ClickInterface clickListener;
    public SearchAdaptor(String[] name) {

        this.name=name;
    }

    @Override
    public SearchAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recyclerlayout,parent,false);
        return new MyViewHolder(view);
    }

    public void setClickListener(ClickInterface itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(SearchAdaptor.MyViewHolder holder, int position) {

        holder.itemName_tv.setText(name[position]);

    }

    @Override
    public int getItemCount() {
        return name.length;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView itemName_tv;
        TextView cart_tv;
        public MyViewHolder(View itemView) {
            super(itemView);

            itemName_tv=(TextView)itemView.findViewById(R.id.itemName_tv);
            cart_tv=(TextView)itemView.findViewById(R.id.cart_tv);


            cart_tv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (clickListener != null)
                clickListener.onClick(v, getAdapterPosition());
        }
    }
}
