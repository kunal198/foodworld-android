package foodworldapp.brst.com.foodworld.adaptors;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class AmountAdaptor extends RecyclerView.Adapter<AmountAdaptor.MyViewHolder>{

    String name[];

    ClickInterface clickListener;

    public AmountAdaptor(String[] name) {

        this.name=name;
    }

    @Override
    public AmountAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_amount,parent,false);
        return new MyViewHolder(view);
    }


    public void setClickListener(ClickInterface itemClickListener) {
        this.clickListener = itemClickListener;
    }


    @Override
    public void onBindViewHolder(AmountAdaptor.MyViewHolder holder, final int position) {

        holder.amountList_tv.setText(name[position]);


        /*if(position==name.length-1)
        {
            holder.viewdivider.setVisibility(View.GONE);
        }*/

    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView amountList_tv;
        View viewdivider;
        public MyViewHolder(View itemView) {
            super(itemView);

            amountList_tv=(TextView)itemView.findViewById(R.id.amountList_tv);
           // viewdivider=(View) itemView.findViewById(R.id.viewdivider);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (clickListener != null)
                clickListener.onClick(v, getAdapterPosition());

        }
    }
}
