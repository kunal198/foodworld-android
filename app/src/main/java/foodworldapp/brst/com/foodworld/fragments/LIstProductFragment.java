package foodworldapp.brst.com.foodworld.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.adaptors.AmountAdaptor;
import foodworldapp.brst.com.foodworld.adaptors.ListProductAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;


public class LIstProductFragment extends Fragment implements View.OnClickListener{

    String name[] = {"Viceroy 750ml", "Amarula 750ml", "Golden Pilsener Lager Can 330ML",
            "Robertson Winery Natural Sweet Red 750ml", "Schweppes Lemonade 200ml",
            "J.Walker Red Label 750ml", "Chivas Regeel 12yrs"};



    RecyclerView listProductRecycler_rv;
    ItemListFragment itemListFragment;
    ShoppingListFragment shoppingListFragment;
    ImageView editProductList_iv,createList_iv;
    TextView amount_tv;

    public LIstProductFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_list_product, container, false);

        BIND_VIEWS(view);

        SET_ADAPTOR();


        return view;
    }

    private void BIND_VIEWS(View view) {

        listProductRecycler_rv = (RecyclerView) view.findViewById(R.id.listProductRecycler_rv);
        editProductList_iv=(ImageView)view.findViewById(R.id.editProductList_iv);
        createList_iv=(ImageView)view.findViewById(R.id.createList_iv);

        itemListFragment=new ItemListFragment();
        shoppingListFragment=new ShoppingListFragment();

        editProductList_iv.setOnClickListener(this);
        createList_iv.setOnClickListener(this);


    }

    private void SET_ADAPTOR() {

        ListProductAdaptor listProductAdaptor = new ListProductAdaptor(name);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listProductRecycler_rv.setLayoutManager(mLayoutManager);
        listProductRecycler_rv.setAdapter(listProductAdaptor);

        // amountAdaptor.setClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.createList_iv:
                replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(), shoppingListFragment);
                break;

            case R.id.editProductList_iv:
                replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(), itemListFragment);
                break;
        }



    }
}
