package foodworldapp.brst.com.foodworld.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.fragments.SearchProductFragment;

public class HomeActivity extends AppCompatActivity {

    FrameLayout container;
    SearchProductFragment searchProductFragment;
    public    TextView header_tv;
   public LinearLayout LinearLayout_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BIND_VIEWS();

        replaceFragment.REPLACE_FRAGMENT(this,searchProductFragment);
      //  REPLACE_FRAGMENT(searchProductFragment);
    }



    private void BIND_VIEWS() {

        container=(FrameLayout)findViewById(R.id.container);
        header_tv=(TextView)findViewById(R.id.header_tv);
        LinearLayout_ll=(LinearLayout)findViewById(R.id.LinearLayout_ll) ;
        searchProductFragment=new SearchProductFragment();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        int count = getSupportFragmentManager().getBackStackEntryCount();
        Log.d("count",count+"");
        if(count==0)
        {
            finish();
        }
    }
}
