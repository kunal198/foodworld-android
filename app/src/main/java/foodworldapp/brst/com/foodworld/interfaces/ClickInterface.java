package foodworldapp.brst.com.foodworld.interfaces;

import android.view.View;

/**
 * Created by brst-pc89 on 5/25/17.
 */
public interface ClickInterface {

    public void onClick(View view, int position);


}
