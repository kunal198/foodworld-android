package foodworldapp.brst.com.foodworld.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;


public class CreateListFragment extends Fragment {


    public CreateListFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_create_list, container, false);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity)getActivity()).header_tv.setText("MY LIST");
    }
}
