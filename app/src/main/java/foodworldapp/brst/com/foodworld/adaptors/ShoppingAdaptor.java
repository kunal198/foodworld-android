package foodworldapp.brst.com.foodworld.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class ShoppingAdaptor extends RecyclerView.Adapter<ShoppingAdaptor.MyViewHolder> {

    String name[];
    List item;
    ClickInterface clickListener;

    public ShoppingAdaptor(List name) {

        this.item = name;
    }

    @Override
    public ShoppingAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shoppinglist_layout, parent, false);
        return new MyViewHolder(view);
    }

    public void setClickListener(ClickInterface itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(final ShoppingAdaptor.MyViewHolder holder, int position) {

        holder.itemNameList_tv.setText(item.get(position).toString());

        holder.inc_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int increment= Integer.parseInt(holder.incdec_tv.getText().toString().trim());

                increment++;

                holder.incdec_tv.setText(String.valueOf(increment));
            }
        });

        holder.dec_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int decrement= Integer.parseInt(holder.incdec_tv.getText().toString().trim());

                decrement--;

                holder.incdec_tv.setText(String.valueOf(decrement));
            }
        });

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView itemNameList_tv,incdec_tv;
        ImageView remove_iv,inc_iv,dec_iv;

        public MyViewHolder(View itemView) {
            super(itemView);

            itemNameList_tv = (TextView) itemView.findViewById(R.id.itemNameList_tv);
            incdec_tv = (TextView) itemView.findViewById(R.id.incdec_tv);
            remove_iv = (ImageView) itemView.findViewById(R.id.remove_iv);
            inc_iv = (ImageView) itemView.findViewById(R.id.inc_iv);
            dec_iv = (ImageView) itemView.findViewById(R.id.dec_iv);

            //    itemView.setOnClickListener(this);
            remove_iv.setOnClickListener(this);
         //   inc_iv.setOnClickListener(this);
           // dec_iv.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {

            if (clickListener != null)
                clickListener.onClick(v, getAdapterPosition());
        }
    }
}
