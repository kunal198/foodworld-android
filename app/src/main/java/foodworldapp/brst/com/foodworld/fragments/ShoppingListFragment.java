package foodworldapp.brst.com.foodworld.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;

import foodworldapp.brst.com.foodworld.adaptors.ShopListAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingListFragment extends Fragment implements ClickInterface,View.OnClickListener {

    String list[]={"List1","List2","List3"};
   SearchMemberFragment searchMemberFragment;
    LIstProductFragment lIstProductFragment;
    AmountFragment amountFragment;
    ImageView createlist_iv;
     List arrayList=new ArrayList();
    RecyclerView shopList_rv;

    ShopListAdaptor shopListAdaptor;
    public ShoppingListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_shopping_list, container, false);

        BIND_VIEWS(view);

        arrayList.clear();

        for(int i=0;i<list.length;i++)
        {
            arrayList.add(list[i]);
        }
        SET_ADAPTOR();

        SET_LISTENER();


        return view;
    }

    private void SET_LISTENER() {

        createlist_iv.setOnClickListener(this);
    }

    private void BIND_VIEWS(View view) {

        shopList_rv=(RecyclerView)view. findViewById(R.id.shopList_rv);
        createlist_iv=(ImageView)view.findViewById(R.id.createlist_iv);
        searchMemberFragment=new SearchMemberFragment();
        lIstProductFragment=new LIstProductFragment();
        amountFragment=new AmountFragment();

    }

    private void SET_ADAPTOR() {

         shopListAdaptor=new ShopListAdaptor(arrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        shopList_rv.setLayoutManager(mLayoutManager);
        shopList_rv.setAdapter(shopListAdaptor);

        shopListAdaptor.setClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).LinearLayout_ll.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).header_tv.setText("MY SHOPPING LIST");
    }

    @Override
    public void onClick(View view, int position) {


        switch (view.getId())
        {
            case R.id.share_iv:
                replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),searchMemberFragment);
                break;

            case R.id.listlayout_ll:


                replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),lIstProductFragment);
                break;

            case R.id.delete_iv:

                    arrayList.remove(position);
                shopListAdaptor.notifyDataSetChanged();


                break;
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.createlist_iv:
                Bundle bundle = new Bundle();
                bundle.putString("value","value");

                amountFragment.setArguments(bundle);

                replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),amountFragment);
        }

    }
}
