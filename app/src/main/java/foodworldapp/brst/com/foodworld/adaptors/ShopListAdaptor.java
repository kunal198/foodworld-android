package foodworldapp.brst.com.foodworld.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * Created by brst-pc89 on 5/24/17.
 */
public class ShopListAdaptor extends RecyclerView.Adapter<ShopListAdaptor.MyViewHolder>{

    String name[];
    ClickInterface clickListener;
    List arrayList;
    public ShopListAdaptor(List name) {

        this.arrayList=name;
    }

    @Override
    public ShopListAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shoplist,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShopListAdaptor.MyViewHolder holder, int position) {

        holder.prList_tv.setText(arrayList.get(position).toString());

        if(position==arrayList.size()-1)
        {
            holder.viewdivider.setVisibility(View.GONE);
        }

    }

    public void setClickListener(ClickInterface itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView prList_tv;
        ImageView share_iv,delete_iv;
        View viewdivider;
        LinearLayout listlayout_ll;
        public MyViewHolder(View itemView) {
            super(itemView);

            prList_tv=(TextView)itemView.findViewById(R.id.prList_tv);
            share_iv=(ImageView) itemView.findViewById(R.id.share_iv);
            delete_iv=(ImageView) itemView.findViewById(R.id.delete_iv);
            viewdivider=(View) itemView.findViewById(R.id.viewdivider);
            listlayout_ll=(LinearLayout) itemView.findViewById(R.id.listlayout_ll);

            share_iv.setOnClickListener(this);
            listlayout_ll.setOnClickListener(this);
            delete_iv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (clickListener != null)
                clickListener.onClick(v, getAdapterPosition());
        }
    }
}
