package foodworldapp.brst.com.foodworld.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.adaptors.AmountAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class AmountFragment extends Fragment implements View.OnClickListener {


    String name[] = {"Viceroy 750ml"};

    /*, "Amarula 750ml", "Golden Pilsener Lager Can 330ML",
            "Robertson Winery Natural Sweet Red 750ml", "Schweppes Lemonade 200ml",
            "J.Walker Red Label 750ml", "Chivas Regeel 12yrs"};*/

    RecyclerView amountRecycler_rv;
    ItemListFragment itemListFragment;
    ImageView editList_iv;
    TextView amount_tv;


    public AmountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_amount, container, false);

        BIND_VIEWS(view);

        Bundle bundle = getArguments();

        if(bundle!=null)
        {
            String  myValue = bundle.getString("value");
            amountRecycler_rv.setVisibility(View.GONE);
            amount_tv.setText("$0");
        }


        SET_ADAPTOR();


        return view;
    }

    private void BIND_VIEWS(View view) {

        amountRecycler_rv = (RecyclerView) view.findViewById(R.id.amountRecycler_rv);
        editList_iv=(ImageView)view.findViewById(R.id.editList_iv);
        amount_tv=(TextView) view.findViewById(R.id.amount_tv);

        editList_iv.setOnClickListener(this);

        itemListFragment = new ItemListFragment();
    }

    private void SET_ADAPTOR() {

        AmountAdaptor amountAdaptor = new AmountAdaptor(name);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        amountRecycler_rv.setLayoutManager(mLayoutManager);
        amountRecycler_rv.setAdapter(amountAdaptor);

       // amountAdaptor.setClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).LinearLayout_ll.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).header_tv.setText("MY LIST");
    }

  /*  @Override
    public void onClick(View view, int position) {


       // replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(), searchMemberFragment);
    }*/

    @Override
    public void onClick(View v) {

        replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(), itemListFragment);

    }
}
