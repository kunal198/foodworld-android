package foodworldapp.brst.com.foodworld.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import foodworldapp.brst.com.foodworld.R;
import foodworldapp.brst.com.foodworld.activities.HomeActivity;
import foodworldapp.brst.com.foodworld.adaptors.DialogAdaptor;
import foodworldapp.brst.com.foodworld.adaptors.SearchAdaptor;
import foodworldapp.brst.com.foodworld.classes.replaceFragment;
import foodworldapp.brst.com.foodworld.interfaces.ClickInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultFragment extends Fragment implements ClickInterface, View.OnClickListener {

    RecyclerView recycler_view;
    TextView cart_tv, addList_tv, done_tv;
    ShoppingListFragment shoppingListFragment;
    AmountFragment amountFragment;
    Dialog dialog, messDialog;

    String name[] = {"Viceroy 750ml", "Amarula 750ml", "Golden Pilsener Lager Can 330ML",
            "Robertson Winery Natural Sweet Red 750ml", "Schweppes Lemonade 200ml",
            "J.Walker Red Label 750ml", "Chivas Regeel 12yrs"};

    String list[] = {"List1", "List2", "List3"};

    public SearchResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        BIND_VIEWS(view);

        SET_ADAPTOR();


        return view;
    }

    private void BIND_VIEWS(View view) {

        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        cart_tv = (TextView) view.findViewById(R.id.cart_tv);

        shoppingListFragment = new ShoppingListFragment();
        amountFragment = new AmountFragment();
    }

    private void SET_ADAPTOR() {

        SearchAdaptor searchAdaptor = new SearchAdaptor(name);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setAdapter(searchAdaptor);

        searchAdaptor.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((HomeActivity) getActivity()).LinearLayout_ll.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View view, int position) {

        Log.d("position", position + "");

        //  replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),amountFragment);

        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        RecyclerView recycler_rv = (RecyclerView) dialog.findViewById(R.id.recycler_rv);
        DialogAdaptor dialogAdaptor = new DialogAdaptor(list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_rv.setLayoutManager(mLayoutManager);
        recycler_rv.setAdapter(dialogAdaptor);

        addList_tv = (TextView) dialog.findViewById(R.id.addList_tv);
        done_tv = (TextView) dialog.findViewById(R.id.done_tv);
        addList_tv.setOnClickListener(this);
        done_tv.setOnClickListener(this);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addList_tv:

                replaceFragment.REPLACE_FRAGMENT((HomeActivity) getActivity(), amountFragment);
                dialog.dismiss();
                break;

            case R.id.done_tv:

                // replaceFragment.REPLACE_FRAGMENT((HomeActivity)getActivity(),amountFragment);
  /*            new AlertDialog.Builder(getContext())
                        .setTitle("Message").setCancelable(false)
                        .setMessage("Product added successsfully")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete

                            }
                        })
                        .show();*/

                messDialog = new Dialog(getContext());
                messDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                messDialog.setContentView(R.layout.dialog_message);
                messDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                messDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                TextView ok_tv = (TextView) messDialog.findViewById(R.id.ok_tv);

                ok_tv.setOnClickListener(this);
                messDialog.show();

                dialog.dismiss();
                break;


            case R.id.ok_tv:

                messDialog.dismiss();

                break;

        }

    }
}
