package foodworldapp.brst.com.foodworld.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import foodworldapp.brst.com.foodworld.R;

/**
 * Created by brst-pc89 on 5/25/17.
 */
public class DialogAdaptor extends RecyclerView.Adapter<DialogAdaptor.MyViewHolder> {

    String list[];
    public DialogAdaptor(String[] list) {

        this.list=list;
    }

    @Override
    public DialogAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_dialoglayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DialogAdaptor.MyViewHolder holder, int position) {


        holder.dialog_tv.setText(list[position]);
    }

    @Override
    public int getItemCount() {

        return list.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dialog_tv;
        public MyViewHolder(View itemView) {
            super(itemView);

            dialog_tv=(TextView)itemView.findViewById(R.id.dialog_tv);
        }
    }
}
