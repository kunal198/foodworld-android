package foodworldapp.brst.com.foodworld.classes;

import android.widget.TextView;

/**
 * Created by brst-pc89 on 5/26/17.
 */
public class searchBean {

    String name;
    Boolean imageVisibility;

    public Boolean getImageVisibility() {
        return imageVisibility;
    }

    public void setImageVisibility(Boolean imageVisibility) {
        this.imageVisibility = imageVisibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
